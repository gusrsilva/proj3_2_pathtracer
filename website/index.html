<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>
  body {
    padding: 100px;
    width: 1000px;
    margin: auto;
    text-align: left;
    font-weight: 300;
    font-family: 'Open Sans', sans-serif;
    color: #121212;
  }
  h1, h2, h3, h4 {
    font-family: 'Source Sans Pro', sans-serif;
  }
</style>
<title>CS 184 Ray Tracer, Part 1</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Source+Sans+Pro" rel="stylesheet">
</head>

<body>

<h1 align="middle">CS 184: Computer Graphics and Imaging, Spring 2017</h1>
<h1 align="middle">Project 3-2: Ray Tracer, Part 2</h1>
<h2 align="middle">Gustavo Silva, CS184-aeq</h2>

<br><br>

<div>

<h2 align="middle">Overview</h2>
<p>In this project we finish up our pathtracer. We do so by adding to our
  repertoire of BRDFs, implementing the Microfacet Model, adding the ability to
  use environment lighting, and adding lens effects to our viewer. Due to the
  fact that path tracing is rooted in the true physical happenings of a scene,
  once we finished up our pathtracer, we were able to create strinkingly realistic
  and detailed scenes.

<h2 align="middle">Part 1: Mirror and Glass Materials</h2>
<p>
  Previously, our path tracer was only able to render scenes with diffuse materials.
  In this section, we added mirror and glass BRDFs. Implementing the mirror was
  quite straight forward since this only deals with perfect reflections. The glass
  proved a bit more complicated though. This is because glass not only reflects
  light rays but also refracts them. In order to deal with the refraction we used
  an approximation of the Fresnel equations known as Shlick's approximation. This allowed
  us to simulate the refraction of light through glass with great detail.
</p>
  <p>
      Below is a sequence of six images of scene CBspheres.dae rendered with
      max_ray_depth set to 0, 1, 2, 3, 4, 5, and 100. The other settings are the
      same at 256 samples per pixel and 4 samples per light.
  </p>
  <div align="center">
    <table style="width=100%">
      <tr>
        <td align="middle">
          <img src="images/part1/max_ray_depth_0.png" width="480px" />
          <figcaption align="middle">Max Ray Depth: 0</figcaption>
        </td>
        <td align="middle">
          <img src="images/part1/max_ray_depth_1.png" width="480px" />
          <figcaption align="middle">Max Ray Depth: 1</figcaption>
        </td>
      </tr>
      <tr>
        <td align="middle">
          <img src="images/part1/max_ray_depth_2.png" width="480px" />
          <figcaption align="middle">Max Ray Depth: 2</figcaption>
        </td>
        <td align="middle">
          <img src="images/part1/max_ray_depth_3.png" width="480px" />
          <figcaption align="middle">Max Ray Depth: 3</figcaption>
        </td>
      </tr>
      <tr>
        <td align="middle">
          <img src="images/part1/max_ray_depth_4.png" width="480px" />
          <figcaption align="middle">Max Ray Depth: 4</figcaption>
        </td>
        <td align="middle">
          <img src="images/part1/max_ray_depth_5.png" width="480px" />
          <figcaption align="middle">Max Ray Depth: 5</figcaption>
        </td>
      </tr>
      <tr>
        <td align="middle" colspan="3">
          <img src="images/part1/max_ray_depth_100.png" width="480px" />
          <figcaption align="middle">Max Ray Depth: 100</figcaption>
        </td>
      </tr>
    </table>
  </div>
  <p>
    We can see that in the first image, no bounces are allowed and thus no
    reflecting or refracting occur and the spheres are blacked out. We notice in
    the second image that reflections only need one bounce to occur and so with
    a max ray depth of 1 we already get a pretty accurate mirror ball. The
    refraction and reflection of the glass ball leads to complicated light paths
    and thus it needs more light bounces until it begins to look accurate.
  </p>

<h2 align="middle">Part 2: Microfacet Material</h2>
<p>
  In this section we implemented isotropic rough conductors that only reflect.
  This was to get us familiar with the Microfacet model. The Microfacet Model
  assumes that surfaces are composed of many small facets, each of which is a mirror,
  or perfect specular material.
</p>
<p>
  A parameter of microfacets we can tune is the "roughness" of the material. The
  roughness parameter, alpha, determines how far each microfacets normal can
  vary from the normal of the smooth surface it is representing.
</p>
<p>
  Below is a sequence of 4 images of scene CBdragon_microfacet_au.dae rendered
  with α set to 0.005, 0.05, 0.25 and 0.5. The other settings are constant at
  256 samples per pixel and 4 samples per light. The max ray depth is also
  constant for all images at 7.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part2/alpha_005.png" width="480px" />
        <figcaption align="middle">alpha: 0.005</figcaption>
      </td>
      <td align="middle">
        <img src="images/part2/alpha_05.png" width="480px" />
        <figcaption align="middle">alpha: 0.05</figcaption>
      </td>
    </tr>
    <tr>
      <td align="middle">
        <img src="images/part2/alpha_25.png" width="480px" />
        <figcaption align="middle">alpha: 0.25</figcaption>
      </td>
      <td align="middle">
        <img src="images/part2/alpha_5.png" width="480px" />
        <figcaption align="middle">alpha: 0.5</figcaption>
      </td>
    </tr>
  </table>
</div>
<p>
  We can see that the lower alpha means the surface normals are closer to that of
  a smooth surface and so overall we have more of a mirror effect. As alpha increase
  we get close to a diffuse effect since light begins to be scattered in many
  different directions.
</p>
<p>
  We also implemented importance sampling for the Microfaced BRDF. Originally,
  we sample a direction on the hemisphere using cosine hemisphere sampling.
  This is not suited for the Beckmann distribution we use for the microfacet
  BRDF though. Thus we importance sample according to the Beckmann distribution
  in an attempt to reduce the variance and noise with less samples.
</p>
<p>
  Below we see a comparison of cosine hemisphere sampling vs importance sampling.
  The sampling rate is fixed at 64 samples per pixel and 1 sample per light and
  the max ray depth for both images is 6.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part2/cos_hem.png" width="480px" />
        <figcaption align="middle">Cosine Hemisphere Sampling</figcaption>
      </td>
      <td align="middle">
        <img src="images/part2/importance.png" width="480px" />
        <figcaption align="middle">Importance Sampling</figcaption>
      </td>
    </tr>
  </table>
</div>
<p>
  As we can see, importance sampling allows much earlier convergence and so we
  achieve better results in the same amount of samples, whereas cosine hemisphere
  sampling would need much more samples to achieve the same results.
</p>
<p>
  If we use even more samples for importance sampling, we get great results:
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part2/importance_1024.png" width="480px" />
      </td>
    </tr>
  </table>
</div>

<p>
  Using the provided website, we were also able to simulate more materials than
  just silver, copper, and gold. Below is an example of our dragon in Titanium.
  The values I used, pulled from the website, are:
  <p align="middle"><pre align="middle">eta = (2.0147, 1.8715, 1.5214)</pre></p>
  <p align="middle"><pre align="middle">k = (.1046, 3.0194, 3.0078)</pre></p>
  I created two versions for comparison, one with low alpha of about 0.001, and
  one with a higher alpha of about 0.3.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part2/dragon_titanium.png" width="480px" />
        <figcaption align="middle">alpha: 0.001</figcaption>
      </td>
      <td align="middle">
        <img src="images/part2/dragon_titanium2.png" width="480px" />
        <figcaption align="middle">alpha 0.3</figcaption>
      </td>
    </tr>
  </table>
</div>

<h2 align="middle">Part 3: Environment Light</h2>
<p>
  In this section we add a new type of light source known as an infinite
  environment light. The idea is that we want to simulate realistic lighting
  environments in the real world. We can achieve this by having a light that
  lights the scene from all directions on the hemisphere. We model this by using
  a light source that is "infinitely far" away. This is done by using a texture
  map paramaterized by theta and phi that give us the intensity of incoming light
  from each direction on the hemisphere.
</p>

<p>
  Below is the doge.exr file I used and under it is the probability_debug.png file
  generated using the save_probability_debug() helper function after
  initializing my probability distributions.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part3/doge.jpg" width="600px" />
      </td>
    </tr>
    <tr>
      <td align="middle">
        <img src="images/part3/probability_debug.png" width="600px" />
      </td>
    </tr>
  </table>
</div>

<p>
  Again, we can make use of importance sampling. This is because most of the light
  provided by the environment light source is concentrated in certain areas. Thus,
  we implement importance sampling by biasing our selection of sampled distances
  towards the areas of higher concentration in an effort to reduce variance and
  noise.
</p>
<p>
   Below is the bunny_unlit.dae scene and the doge.exr environment map file, one
   image is rendered using uniform sampling and one using importance sampling.
   The other settings are constant at 4 samples per pixel and 64 samples per
   light in each.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part3/unlit_uniform.png" width="480px" />
        <figcaption align="middle">Uniform Sampling</figcaption>
      </td>
      <td align="middle">
        <img src="images/part3/unlit_importance.png" width="480px" />
        <figcaption align="middle">Importance Sampling</figcaption>
      </td>
    </tr>
  </table>
</div>

<p>
  Below is the bunny_microfacet_cu_unlit.dae scene and the doge.exr environment
  map. One uses uniform sampling and the other uses importance sampling. The
  other settings are constant at 4 samples per pixel and 64 samples per light.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part3/microfacet_unlit_uniform.png" width="480px" />
        <figcaption align="middle">Uniform Sampling</figcaption>
      </td>
      <td align="middle">
        <img src="images/part3/microfacet_unlit_importance.png" width="480px" />
        <figcaption align="middle">Importance Sampling</figcaption>
      </td>
    </tr>
  </table>
</div>
<p>
  To be honest, there seems to be very little difference in noise between the two
  sampling techniques. If I had to guess, I would say it is because the number
  of samples we are asked to use (4 samples per pixel and 64 samples per light)
  is enough for the uniform sampling to converge. Perhaps we would see a bigger
  difference if we used less samples so that uniform did not converge but the
  properties of importance sampling allowed it to converge.
</p>

<h2 align="middle">Part 4: Depth of Field</h2>
<p>
  Up until now we have been rendering our scenes as though the camera is an ideal
  pinhole camera. A nice way to add a bit of realism to our rendering though,
  is to simulate the scene being viewed though a lens. Specifically, for us,
  this allows us to add depth of field to our images and leads to some
  beautiful results.
</p>
<p>
  Essentially, with a pinhole camera each pixel could only recieve light from one
  point in the scene. However, due to a lens refractive properties, a pixel can
  now recieve light from more points on the scene. Thus, we update our sampling
  to also include samples from all points in the scene where a pixel could
  receive light from, depending on the focal length and aperture size parameters.
</p>
<p>
  Below is a "focus stack", where we are focusing at 4 visibly different depths
  through a scene, created by changing the focal distance by .2 at each step.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part4/focus_1.png" width="480px" />
      </td>
      <td align="middle">
        <img src="images/part4/focus_2.png" width="480px" />
      </td>
    </tr>
    <tr>
      <td align="middle">
        <img src="images/part4/focus_3.png" width="480px" />
      </td>
      <td align="middle">
        <img src="images/part4/focus_4.png" width="480px" />
      </td>
    </tr>
  </table>
</div>
<p>
  We can see that at each image a different section of the dragon comes in to focus
  as the focus plane sweeps away from the camera.
</p>
<p>
  Below is a sequence of 4 pictures with visibly different aperture sizes, all
  focused at the same point in a scene, the mirror ball on the left side of the
  scene.
</p>
<div align="center">
  <table style="width=100%">
    <tr>
      <td align="middle">
        <img src="images/part4/aperture_1.png" width="480px" />
      </td>
      <td align="middle">
        <img src="images/part4/aperture_2.png" width="480px" />
      </td>
    </tr>
    <tr>
      <td align="middle">
        <img src="images/part4/aperture_3.png" width="480px" />
      </td>
      <td align="middle">
        <img src="images/part4/aperture_4.png" width="480px" />
      </td>
    </tr>
  </table>
</div>
<p>
  Here we can see that in the beginning, the aperture is quite small and so we have
  a wide ranging depth of field and as a result a majority of the scene is in
  focus. However, as we increase the aperture size, more and more areas of the
  scene can contribute to each pixel, and so our depth of field becomes more
  and more narrow, causing less and less of the scene to be in focus.
</p>

</div>
</body>
</html>
