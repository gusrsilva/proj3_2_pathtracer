#include "bsdf.h"

#include <iostream>
#include <algorithm>
#include <utility>

using std::min;
using std::max;
using std::swap;

namespace CGL {

    void make_coord_space(Matrix3x3 &o2w, const Vector3D &n) {

        Vector3D z = Vector3D(n.x, n.y, n.z);
        Vector3D h = z;
        if (fabs(h.x) <= fabs(h.y) && fabs(h.x) <= fabs(h.z)) h.x = 1.0;
        else if (fabs(h.y) <= fabs(h.x) && fabs(h.y) <= fabs(h.z)) h.y = 1.0;
        else h.z = 1.0;

        z.normalize();
        Vector3D y = cross(h, z);
        y.normalize();
        Vector3D x = cross(z, y);
        x.normalize();

        o2w[0] = x;
        o2w[1] = y;
        o2w[2] = z;
    }


// Diffuse BSDF //

    Spectrum DiffuseBSDF::f(const Vector3D &wo, const Vector3D &wi) {
        // Part 3, Task 1:
        // This function takes in both wo and wi and returns the evaluation of
        // the BSDF for those two directions.
        return this->reflectance * (1.0 / PI);
    }


    Spectrum DiffuseBSDF::sample_f(const Vector3D &wo, Vector3D *wi, float *pdf) {
        // Part 3, Task 1:
        // This function takes in only wo and provides pointers for wi and pdf,
        // which should be assigned by this function.
        // After sampling a value for wi, it returns the evaluation of the BSDF
        // at (wo, *wi).
        *pdf = (float) (1.0 / (PI));
        *wi = this->sampler.get_sample(pdf);
        return this->f(wo, *wi);
    }


// Mirror BSDF //

    Spectrum MirrorBSDF::f(const Vector3D &wo, const Vector3D &wi) {
        return Spectrum();
    }

    Spectrum MirrorBSDF::sample_f(const Vector3D &wo, Vector3D *wi, float *pdf) {

        // TODO: 3-2 Part 1 Task 2
        // Implement MirrorBSDF
        *pdf = 1;
        reflect(wo, wi);
        return this->reflectance / abs_cos_theta(*wi);
    }


// Microfacet BSDF //
    Spectrum MicrofacetBSDF::F(const Vector3D &wi) {
        // TODO: proj3-2, part 2
        // Compute Fresnel term for reflection on dielectric-conductor interface.
        // You will need both eta and etaK, both of which are Spectrum.
        Spectrum n2_k2 = eta*eta + k*k;
        double cos0 = cos_theta(wi);

        Spectrum Rs_num = (n2_k2) - 2.0*eta*cos0 + pow(cos0, 2.0);
        Spectrum Rs_denom = (n2_k2) + 2.0*eta*cos0 + pow(cos0, 2.0);

        Spectrum Rp_num = (n2_k2)*pow(cos0, 2.0) - 2*eta*cos0 + 1.0;
        Spectrum Rp_denom = (n2_k2)*pow(cos0, 2.0) + 2*eta*cos0 + 1.0;

        Spectrum Rs = Rs_num/Rs_denom;
        Spectrum Rp = Rp_num/Rp_denom;

        return 0.5*(Rs+Rp);
    }

    double MicrofacetBSDF::G(const Vector3D &wo, const Vector3D &wi) {
        return 1.0 / (1.0 + Lambda(wi) + Lambda(wo));
    }

    double MicrofacetBSDF::D(const Vector3D &h) {
        // TODO: proj3-2, part 2
        // Compute Beckmann normal distribution function (NDF) here.
        // You will need the roughness alpha.
        double theta = getTheta(h);
        double num = exp(-pow(tan(theta), 2.0)/pow(alpha, 2.0));
        double denom = M_PI*pow(alpha, 2.0)*std::pow(cos_theta(h), 4.0);
        return num/denom;
    }

    Spectrum MicrofacetBSDF::f(const Vector3D &wo, const Vector3D &wi) {
        // TODO: proj3-2, part 2
        // Implement microfacet model here.
        if(wo.z <= 0 or wi.z <= 0){
            return Spectrum();
        }
        Vector3D h = (wo + wi).unit();
        return (F(wi)*G(wo, wi)*D(h))/(4*wo.z*wi.z);
    }

    Spectrum MicrofacetBSDF::sample_f(const Vector3D &wo, Vector3D *wi, float *pdf) {
        // TODO: proj3-2, part 2
        // *Importance* sample Beckmann normal distribution function (NDF) here.
        // Note: You should fill in the sampled direction *wi and the corresponding *pdf,
        //       and return the sampled BRDF value.
        bool cosHemSample = false;
        if(cosHemSample) {
            *wi = cosineHemisphereSampler.get_sample(pdf);
            return MicrofacetBSDF::f(wo, *wi);
        }
        else {
            Vector2D r = sampler.get_sample();
            double theta = atan(sqrt(-pow(alpha, 2.0)*log(1-r.x)));
            double phi  =2*M_PI*r.y;

            // TODO: Check this! Swap theta, phi; r = 1 ; normalize
            Vector3D h = Vector3D(cos(phi)*sin(theta), sin(phi)*sin(theta), cos(theta));

            *wi = (-wo + 2 * dot(wo, h) * h).unit();

            bool invalidH = theta <= 0 or theta >= 0.5*M_PI or phi <= 0 or phi >= 2*M_PI;
            if(wi->z <= 0 or invalidH) {
                *pdf = 0;
                return Spectrum();
            }

            double p_theta_num = (2*sin(theta)*exp(-pow(tan(theta), 2.0)/pow(alpha, 2.0)));
            double p_theta_denom = pow(alpha, 2.0)*pow(cos(theta), 3.0);

            double p_theta = p_theta_num/p_theta_denom;
            double p_phi = 1.0/(2*M_PI);

            double p_w_h = (p_theta*p_phi)/sin(theta);
            double p_w_wi = p_w_h/(4*dot(*wi, h));

            *pdf = (float) p_w_wi;

            return MicrofacetBSDF::f(wo, *wi);
        }
    }


// Refraction BSDF //

    Spectrum RefractionBSDF::f(const Vector3D &wo, const Vector3D &wi) {
        return Spectrum();
    }

    Spectrum RefractionBSDF::sample_f(const Vector3D &wo, Vector3D *wi, float *pdf) {


        return Spectrum();
    }

// Glass BSDF //

    Spectrum GlassBSDF::f(const Vector3D &wo, const Vector3D &wi) {
        return Spectrum();
    }

    Spectrum GlassBSDF::sample_f(const Vector3D &wo, Vector3D *wi, float *pdf) {

        // TODO: 3-2 Part 1 Task 4
        // Compute Fresnel coefficient and either reflect or refract based on it.
        bool totalInternalReflection = !refract(wo, wi, this->ior);
        if (totalInternalReflection) {
            reflect(wo, wi);
            *pdf = 1;
            return reflectance / abs_cos_theta(*wi);
        }
        double eta;
        if (wo.z >= 0) {
            eta = 1.0 / ior;
        } else {
            eta = ior;
        }

        double R0 = pow(((1 - eta) / (1 + eta)), 2);
        double R = R0 + (1 - R0) * pow(1 - abs_cos_theta(*wi), 5);
        if (coin_flip(R)) {
            reflect(wo, wi);
            *pdf = R;
            return R * reflectance / abs_cos_theta(*wi);
        } else {
            refract(wo, wi, this->ior);
            *pdf = 1 - R;
            return (1 - R) * transmittance / abs_cos_theta(*wi) / pow(eta, 2);
        }
    }

    void BSDF::reflect(const Vector3D &wo, Vector3D *wi) {

        // TODO: 3-2 Part 1 Task 1
        // Implement reflection of wo about normal (0,0,1) and store result in wi.
        *wi = Vector3D(-wo[0], -wo[1], wo[2]);

    }

    bool BSDF::refract(const Vector3D &wo, Vector3D *wi, float ior) {

        // TODO: 3-2 Part 1 Task 3
        // Use Snell's Law to refract wo surface and store result ray in wi.
        // Return false if refraction does not occur due to total internal reflection
        // and true otherwise. When wo.z is positive, then wo corresponds to a
        // ray entering the surface through vacuum.

        // When wo.z is positive we are entering
        // n = 1/ior when entering, n = ior otherwise
        // wi.z=(opposite sign of wo.z)*sqrt(1−n^2(1−wo.z^2))

        double eta;
        if (wo.z >= 0) {
            eta = 1.0 / ior;
        } else {
            eta = ior;
        }
        double determinant = 1.0 - pow(eta, 2) * (1.0 - pow(wo.z, 2));
        if (determinant < 0) {
            // Total internal reflection
            return false;
        }
        int sign = -1;
        if (wo.z < 0) {
            sign = 1;
        }
        wi->x = -eta * wo.x;
        wi->y = -eta * wo.y;
        wi->z = sign * sqrt(determinant);

        return true;

    }

// Emission BSDF //

    Spectrum EmissionBSDF::f(const Vector3D &wo, const Vector3D &wi) {
        return Spectrum();
    }

    Spectrum EmissionBSDF::sample_f(const Vector3D &wo, Vector3D *wi, float *pdf) {
        *pdf = 1.0 / PI;
        *wi = sampler.get_sample(pdf);
        return Spectrum();
    }

} // namespace CGL
