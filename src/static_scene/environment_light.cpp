#include "environment_light.h"

#include <algorithm>
#include <iostream>
#include <fstream>

namespace CGL { namespace StaticScene {

EnvironmentLight::EnvironmentLight(const HDRImageBuffer* envMap)
    : envMap(envMap) {
    	init();
}

EnvironmentLight::~EnvironmentLight() {
    delete[] pdf_envmap;
    delete[] conds_y;
    delete[] marginal_y;
}


void EnvironmentLight::init() {
	uint32_t w = envMap->w, h = envMap->h;
    pdf_envmap = new double[w * h];
	conds_y = new double[w * h];
	marginal_y = new double[h];

    double p[h];

	std::cout << "[PathTracer] Initializing environment light...";

	// TODO 3-2 Part 3 Task 3 Steps 1,2,3
	// Store the environment map pdf to pdf_envmap
    double denom = 0.0;
    for(int i = 0; i < w; i++) {
        for(int j=0; j < h; j++) {
            denom += (envMap->data[j*w + i].illum())*sin(M_PI*(j+0.5)/h);
        }
    }
    for(int x=0; x < w; x++) {
        for(int y=0; y < h; y++) {
            int index = y*w + x;
            pdf_envmap[index] = (envMap->data[index].illum())*sin(M_PI*(y+0.5)/h)/denom;
        }
    }
	// Store the marginal distribution for y to marginal_y
    double runningSum = 0.0;
    for(int j =0; j < h; j++) {
        p[j] = 0.0;
        for(int i=0; i < w; i++) {  // TODO: i < w or w-1 ?
            p[j] += pdf_envmap[j*w + i];
        }
        runningSum += p[j];
        marginal_y[j] = runningSum;
    }

	// Store the conditional distribution for x given y to conds_y
    for(int j=0; j < h; j++) {
        double sum = 0.0;
        for(int i=0; i < w; i++) {
            sum += pdf_envmap[j*w + i];
            if(p[j] <= 0) {
                conds_y[j*w + i] = 0.0;
            }
            else {
                conds_y[j * w + i] = sum / p[j];
            }
        }
    }

	if (true)
		save_probability_debug();

	std::cout << "done." << std::endl;
}


// Helper functions

void EnvironmentLight::save_probability_debug() {
	uint32_t w = envMap->w, h = envMap->h;
	uint8_t* img = new uint8_t[4*w*h];

	for (int j = 0; j < h; ++j) {
		for (int i = 0; i < w; ++i) {
			img[4 * (j * w + i) + 3] = 255;
			img[4 * (j * w + i) + 0] = 255 * marginal_y[j];
			img[4 * (j * w + i) + 1] = 255 * conds_y[j * w + i];
		}
	}

    lodepng::encode("probability_debug.png", img, w, h);
    delete[] img;
}

Vector2D EnvironmentLight::theta_phi_to_xy(const Vector2D &theta_phi) const {
    uint32_t w = envMap->w, h = envMap->h;
    double x = theta_phi.y / 2. / M_PI * w;
    double y = theta_phi.x / M_PI * h;
    return Vector2D(x, y);
}

Vector2D EnvironmentLight::xy_to_theta_phi(const Vector2D &xy) const {
    uint32_t w = envMap->w, h = envMap->h;
    double x = xy.x;
    double y = xy.y;
    double phi = x / w * 2.0 * M_PI;
    double theta = y / h * M_PI;
    return Vector2D(theta, phi);
}

Vector2D EnvironmentLight::dir_to_theta_phi(const Vector3D &dir) const {
    dir.unit();
    double theta = acos(dir.y);
    double phi = atan2(-dir.z, dir.x) + M_PI;
    return Vector2D(theta, phi);
}

Vector3D EnvironmentLight::theta_phi_to_dir(const Vector2D& theta_phi) const {
    double theta = theta_phi.x;
    double phi = theta_phi.y;

    double y = cos(theta);
    double x = cos(phi - M_PI) * sin(theta);
    double z = -sin(phi - M_PI) * sin(theta);

    return Vector3D(x, y, z);
}

Spectrum EnvironmentLight::bilerp(const Vector2D& xy) const {
	uint32_t w = envMap->w;
	const std::vector<Spectrum>& data = envMap->data;
	double x = xy.x, y = xy.y;
	Spectrum ret;
	for (int i = 0; i < 4; ++i)
		ret += (i%2 ? x-floor(x) : ceil(x)-x) * 
			   (i/2 ? y-floor(y) : ceil(y)-y) * 
			   data[w * (floor(y) + i/2) + floor(x) + i%2];
	return ret;
}


Spectrum EnvironmentLight::sample_L(const Vector3D& p, Vector3D* wi,
                                    float* distToLight,
                                    float* pdf) const {
  // TODO: 3-2 Part 3 Tasks 2 and 3 (step 4)
    bool uniformSampling = false;
	// First implement uniform sphere sampling for the environment light
    if(uniformSampling) {
        Vector3D uSample = sampler_uniform_sphere.get_sample();
        *wi = uSample;
        Vector2D thetaPhi = dir_to_theta_phi(uSample);

        *distToLight = INF_F;
        *pdf = (float) (1 / (4 * PI));
        return bilerp(theta_phi_to_xy(thetaPhi));
    }
    else {
        // Later implement full importance sampling
        Vector2D sample = sampler_uniform2d.get_sample();
        size_t w(envMap->w), h(envMap->h);
        int y = (int) (std::upper_bound(&marginal_y[0], &marginal_y[h], sample.y) - &marginal_y[0]);
        int x = (int) (std::upper_bound(&conds_y[y * w], &conds_y[y * w + w], sample.x) - &conds_y[y * w]);

        Vector2D xy = Vector2D(x, y);
        Vector2D theta_phi = xy_to_theta_phi(xy);
        if(theta_phi.x <= 0 or theta_phi.x >= 0.5*M_PI or theta_phi.y <= 0 or theta_phi.y >= 2*M_PI) {
            *pdf = 0;
            return Spectrum();
        }
        *wi = theta_phi_to_dir(theta_phi).unit();
        *distToLight = INF_F;
        *pdf = (float) (pdf_envmap[y*w + x] * (w * h) / (2 * M_PI * M_PI * sin(theta_phi.x)));
        return envMap->data[y*w+x];
    }
}

Spectrum EnvironmentLight::sample_dir(const Ray& r) const {
  // TODO: 3-2 Part 3 Task 1
	// Use the helper functions to convert r.d into (x,y)
	// then bilerp the return value
    Vector2D theta_phi = dir_to_theta_phi(r.d);
    Vector2D xy = theta_phi_to_xy(theta_phi);
    return bilerp(xy);
}

} // namespace StaticScene
} // namespace CGL
